---
status: in development
product: first prototype evaluation
---
# ePlus Pcb

ModBus RTU with power supply Sensor convertor.

![image.png](./image.png)

## galvanic isolation:

5 Pin screw terminal connector 

- RS485 A,B, Earth
- Power Supply 8..48V IN+ and IN-

Connect IN- with Earth on one Star Point.

RS-485 driver is connected to the IN-

RS485 with auto Transmit detection=> half Duplex=> 2-wire

Earth is used as Flash protector, with Termal recovery fuses and TVS diodes for Power and RTU AB

## MEGA328P

- with ICSP (SPI) programmer
- 3V3 / 8Mhz
- Hardware serial for RS485
- NeoCiSerial for (D8/D9)
    - RS232 (j3)
    - PM2.5 sensor(j10/j13)
    - Debug (J7)
- SPI for BME280 TPH sensor (D10 CS)
- IIC (SDA/SCL) (A4/A5)
    - oled (J6)
    - MCSJ811 (J5) => eCO2
- Analog => 0..5V, with 5V power supply 3 pole screw terminals
    - J11 Analog 0 (A0)
    - J12 Analog 1 (A1)
- Pulse input => 3 pole screwterminal
    - J8 => 3V3 OR 1-wire for DS812 INT0
    - J9 => 5V (e.g. Water Flow sensor) INT1


