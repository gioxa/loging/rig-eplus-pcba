EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RiG-ePlus"
Date "2021-09-15"
Rev ""
Comp "Gioxa Ltd"
Comment1 "Danny Goossen"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U1
U 1 1 613BFB4B
P 2400 7050
F 0 "U1" V 2300 6250 50  0000 C CNN
F 1 "ATmega328P-AU" V 2100 6500 50  0000 C CNN
F 2 "Package_QFP:LQFP-32_7x7mm_P0.8mm" H 2400 7050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 2400 7050 50  0001 C CNN
	1    2400 7050
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 613C46E1
P 10100 5800
F 0 "J1" H 10150 6117 50  0000 C CNN
F 1 "ICSP" H 10150 6026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 10100 5800 50  0001 C CNN
F 3 "~" H 10100 5800 50  0001 C CNN
	1    10100 5800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 613C6446
P 10800 5600
F 0 "#PWR0101" H 10800 5450 50  0001 C CNN
F 1 "+3.3V" H 10815 5773 50  0000 C CNN
F 2 "" H 10800 5600 50  0001 C CNN
F 3 "" H 10800 5600 50  0001 C CNN
	1    10800 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 613C729D
P 10800 6000
F 0 "#PWR0102" H 10800 5750 50  0001 C CNN
F 1 "GND" H 10805 5827 50  0000 C CNN
F 2 "" H 10800 6000 50  0001 C CNN
F 3 "" H 10800 6000 50  0001 C CNN
	1    10800 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 5900 10800 6000
Wire Wire Line
	10400 5900 10800 5900
Wire Wire Line
	10400 5700 10800 5700
Wire Wire Line
	10800 5700 10800 5600
Wire Wire Line
	10400 5800 10650 5800
Text Label 10450 5800 0    50   ~ 0
MOSI
Wire Wire Line
	9900 5700 9700 5700
Wire Wire Line
	9900 5800 9700 5800
Wire Wire Line
	9900 5900 9700 5900
Text Label 9750 5700 0    50   ~ 0
MISO
Text Label 9750 5800 0    50   ~ 0
SCK
Text Label 9750 5900 0    50   ~ 0
RESET
Wire Wire Line
	1100 6700 1100 6450
Text Label 1100 6400 1    50   ~ 0
D8
Text Label 1200 6400 1    50   ~ 0
D9
Text Label 1300 6400 1    50   ~ 0
D10-SS
Text Label 1400 6400 1    50   ~ 0
D11-MOSI
Text Label 1500 6400 1    50   ~ 0
D12-MISO
Text Label 1600 6400 1    50   ~ 0
D13-SCK
Text Label 2400 6350 1    50   ~ 0
A4-SDA
Text Label 2500 6350 1    50   ~ 0
A5-SCL
Text Label 2600 6350 1    50   ~ 0
RESET
Wire Wire Line
	3300 6700 3300 6450
Text Label 3000 6350 1    50   ~ 0
RXI
Text Label 3100 6350 1    50   ~ 0
TXO
Text Label 3200 6350 1    50   ~ 0
D2-INT0
Text Label 3300 6350 1    50   ~ 0
D3-INT1
Text Label 3400 6350 1    50   ~ 0
D4-T0
Text Label 3500 6350 1    50   ~ 0
D5-T1
Text Label 3600 6350 1    50   ~ 0
D6
Text Label 3700 6350 1    50   ~ 0
D7
$Comp
L power:+3.3V #PWR0103
U 1 1 61426241
P 800 6000
F 0 "#PWR0103" H 800 5850 50  0001 C CNN
F 1 "+3.3V" H 815 6173 50  0000 C CNN
F 2 "" H 800 6000 50  0001 C CNN
F 3 "" H 800 6000 50  0001 C CNN
	1    800  6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 61428106
P 4050 7500
F 0 "#PWR0104" H 4050 7250 50  0001 C CNN
F 1 "GND" H 4055 7327 50  0000 C CNN
F 2 "" H 4050 7500 50  0001 C CNN
F 3 "" H 4050 7500 50  0001 C CNN
	1    4050 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6450 3400 5950
Wire Wire Line
	2600 6450 2600 6050
Wire Wire Line
	3200 6450 3200 6000
$Comp
L Isolator:ADuM121N U3
U 1 1 614219B0
P 8450 1800
F 0 "U3" H 8800 2200 50  0000 L CNN
F 1 "ADuM3201" H 8800 1400 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8450 1100 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADuM120N_121N.pdf" H 8000 2200 50  0001 C CNN
	1    8450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 1900 8950 1900
Wire Wire Line
	9000 1700 8950 1700
Wire Wire Line
	7950 1700 7350 1700
Wire Wire Line
	7350 1700 7350 1500
$Comp
L power:GND #PWR0106
U 1 1 61447EA3
P 9000 2500
F 0 "#PWR0106" H 9000 2250 50  0001 C CNN
F 1 "GND" H 9005 2327 50  0000 C CNN
F 2 "" H 9000 2500 50  0001 C CNN
F 3 "" H 9000 2500 50  0001 C CNN
	1    9000 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0107
U 1 1 614492BA
P 9000 1150
F 0 "#PWR0107" H 9000 1000 50  0001 C CNN
F 1 "+3.3V" H 9015 1323 50  0000 C CNN
F 2 "" H 9000 1150 50  0001 C CNN
F 3 "" H 9000 1150 50  0001 C CNN
	1    9000 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1300 8550 1400
Wire Wire Line
	8550 2200 8550 2300
Wire Wire Line
	8350 1400 8350 1300
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 6148145A
P 10900 1300
F 0 "J3" H 10928 1276 50  0000 L CNN
F 1 "RS232" H 10600 1600 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 10900 1300 50  0001 C CNN
F 3 "~" H 10900 1300 50  0001 C CNN
	1    10900 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 4200 10050 4200
Wire Wire Line
	9850 3600 9850 4200
Wire Wire Line
	10750 3600 9850 3600
Wire Wire Line
	10100 2400 10100 2850
Wire Wire Line
	10700 2400 10100 2400
$Comp
L power:GND #PWR0108
U 1 1 614295F2
P 10100 2850
F 0 "#PWR0108" H 10100 2600 50  0001 C CNN
F 1 "GND" H 10105 2677 50  0000 C CNN
F 2 "" H 10100 2850 50  0001 C CNN
F 3 "" H 10100 2850 50  0001 C CNN
	1    10100 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 61428A16
P 6850 5650
F 0 "#PWR0109" H 6850 5400 50  0001 C CNN
F 1 "GND" H 6855 5477 50  0000 C CNN
F 2 "" H 6850 5650 50  0001 C CNN
F 3 "" H 6850 5650 50  0001 C CNN
	1    6850 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3450 9850 3500
Wire Wire Line
	10100 2300 10700 2300
Wire Wire Line
	10100 2250 10100 2300
$Comp
L power:+3.3V #PWR0110
U 1 1 61426A43
P 9850 3450
F 0 "#PWR0110" H 9850 3300 50  0001 C CNN
F 1 "+3.3V" H 9865 3623 50  0000 C CNN
F 2 "" H 9850 3450 50  0001 C CNN
F 3 "" H 9850 3450 50  0001 C CNN
	1    9850 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0111
U 1 1 61424B0C
P 10100 2250
F 0 "#PWR0111" H 10100 2100 50  0001 C CNN
F 1 "+3.3V" H 10115 2423 50  0000 C CNN
F 2 "" H 10100 2250 50  0001 C CNN
F 3 "" H 10100 2250 50  0001 C CNN
	1    10100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3500 10750 3500
$Comp
L Connector:Conn_01x08_Female J5
U 1 1 6142256B
P 10950 3800
F 0 "J5" H 10978 3776 50  0000 L CNN
F 1 "CJMCU-811" H 10450 4250 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10950 3800 50  0001 C CNN
F 3 "~" H 10950 3800 50  0001 C CNN
	1    10950 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Female J4
U 1 1 6141FC4D
P 10900 2500
F 0 "J4" H 10928 2476 50  0000 L CNN
F 1 "BME280" H 10600 2850 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 10900 2500 50  0001 C CNN
F 3 "~" H 10900 2500 50  0001 C CNN
	1    10900 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6450 3500 5950
$Comp
L Device:R_Small R1
U 1 1 614A79D0
P 7300 6350
F 0 "R1" V 7400 6300 50  0000 L CNN
F 1 "1K" V 7300 6300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 6350 50  0001 C CNN
F 3 "~" H 7300 6350 50  0001 C CNN
	1    7300 6350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 614A8EEC
P 7300 6150
F 0 "R2" V 7400 6100 50  0000 L CNN
F 1 "1K" V 7300 6100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 6150 50  0001 C CNN
F 3 "~" H 7300 6150 50  0001 C CNN
	1    7300 6150
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0112
U 1 1 614B0603
P 7100 6150
F 0 "#PWR0112" H 7100 6000 50  0001 C CNN
F 1 "+3.3V" H 7100 6300 50  0000 C CNN
F 2 "" H 7100 6150 50  0001 C CNN
F 3 "" H 7100 6150 50  0001 C CNN
	1    7100 6150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:MPM3570E U2
U 1 1 614D90D0
P 5450 3750
F 0 "U2" H 5450 4117 50  0000 C CNN
F 1 "MPM3570E" H 5450 4026 50  0000 C CNN
F 2 "MPM3570:MPM3570EGLD" H 5450 4100 50  0001 C CNN
F 3 "https://www.monolithicpower.com/pub/media/document/MP1470_r1.02.pdf" H 5450 3750 50  0001 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3650 6300 3650
Wire Wire Line
	5550 4050 5550 4250
Wire Wire Line
	5550 4250 5450 4250
Wire Wire Line
	5450 4050 5450 4250
Connection ~ 5450 4250
Wire Wire Line
	5350 4050 5350 4250
Wire Wire Line
	5350 4250 5450 4250
Wire Wire Line
	5550 4250 5950 4250
Connection ~ 5550 4250
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 614508E6
P 800 3450
F 0 "J2" H 828 3476 50  0000 L CNN
F 1 "Conn_01x05_Female" H 50  3850 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MCV_1,5_5-G-5.08_1x05_P5.08mm_Vertical" H 800 3450 50  0001 C CNN
F 3 "~" H 800 3450 50  0001 C CNN
	1    800  3450
	-1   0    0    -1  
$EndComp
Connection ~ 5350 4250
Text Label 10500 2500 0    50   ~ 0
SCK
$Comp
L power:+3V3 #PWR0114
U 1 1 614641AD
P 10150 1150
F 0 "#PWR0114" H 10150 1000 50  0001 C CNN
F 1 "+3V3" H 10165 1323 50  0000 C CNN
F 2 "" H 10150 1150 50  0001 C CNN
F 3 "" H 10150 1150 50  0001 C CNN
	1    10150 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 1150 10150 1200
Wire Wire Line
	10150 1200 10700 1200
Wire Wire Line
	10700 1300 10150 1300
Wire Wire Line
	1200 6450 1200 6050
Text Label 10500 1400 0    50   ~ 0
RxD
Text Label 10500 1500 0    50   ~ 0
TxD
Text GLabel 10450 1500 0    50   Input ~ 0
TxD
Text GLabel 10450 1400 0    50   Output ~ 0
RxD
Text GLabel 1200 6050 1    50   Output ~ 0
TxD
Text GLabel 1100 6050 1    50   Input ~ 0
RxD
Wire Wire Line
	10450 1400 10700 1400
Wire Wire Line
	10450 1500 10700 1500
$Comp
L power:GND #PWR0115
U 1 1 6149D275
P 10150 1400
F 0 "#PWR0115" H 10150 1150 50  0001 C CNN
F 1 "GND" H 10155 1227 50  0000 C CNN
F 2 "" H 10150 1400 50  0001 C CNN
F 3 "" H 10150 1400 50  0001 C CNN
	1    10150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 1300 10150 1400
Text Label 10650 1200 2    50   ~ 0
VCC
Text Label 10650 1300 2    50   ~ 0
GND
Text Notes 10600 850  2    50   ~ 0
RS232
Wire Notes Line
	9950 700  11100 700 
Wire Notes Line
	11100 700  11100 1800
Wire Notes Line
	9950 1800 9950 700 
Wire Notes Line
	9950 1800 11100 1800
Text Label 10700 2600 2    50   ~ 0
MOSI
Text Label 10650 2700 2    50   ~ 0
CS
Text Label 10700 2800 2    50   ~ 0
MISO
Text GLabel 10350 2500 0    50   Input ~ 0
SCK
Text GLabel 10400 2800 0    50   Output ~ 0
MISO
Text GLabel 10300 2700 0    50   Input ~ 0
CS
Text GLabel 10400 2600 0    50   Input ~ 0
MOSI
Wire Wire Line
	10350 2500 10700 2500
Wire Wire Line
	10400 2600 10700 2600
Wire Wire Line
	10300 2700 10700 2700
Wire Wire Line
	10700 2800 10400 2800
Text Notes 10750 2000 2    50   ~ 0
BME280 SPI
Wire Notes Line
	9950 1900 11100 1900
Wire Notes Line
	11100 1900 11100 3100
Wire Notes Line
	11100 3100 9950 3100
Wire Notes Line
	9950 3100 9950 1900
Text GLabel 10150 3700 0    50   BiDi ~ 0
SDA
Text GLabel 10150 3800 0    50   Input ~ 0
SCL
Wire Wire Line
	10750 3700 10150 3700
Wire Wire Line
	10750 3800 10150 3800
Text GLabel 7450 6350 2    50   BiDi ~ 0
SDA
Text GLabel 7450 6150 2    50   Input ~ 0
SCL
Text GLabel 10100 3900 0    50   Input ~ 0
D4
Text GLabel 10150 4000 0    50   Output ~ 0
INT1
Wire Wire Line
	10150 4000 10200 4000
Text GLabel 10100 4100 0    50   Input ~ 0
T1
Wire Wire Line
	10100 3900 10750 3900
Wire Wire Line
	10750 4100 10100 4100
Text Label 10600 3700 2    50   ~ 0
SDA
Text Label 10600 3800 2    50   ~ 0
SCL
Text Label 10600 3900 2    50   ~ 0
WAKE
Text Label 10600 4000 2    50   ~ 0
INT
Text Label 10600 4100 2    50   ~ 0
RESET
Text GLabel 2400 6000 1    50   BiDi ~ 0
SDA
Text GLabel 2500 6000 1    50   Output ~ 0
SCL
Text GLabel 3400 5950 1    50   Output ~ 0
D4
Text GLabel 3200 6000 1    50   Input ~ 0
INT0
Text GLabel 3500 5950 1    50   Output ~ 0
T1
Wire Wire Line
	2400 6000 2400 6450
Wire Wire Line
	2500 6000 2500 6450
Text GLabel 1300 5900 1    50   Output ~ 0
CS
Text GLabel 1400 5900 1    50   Output ~ 0
MOSI
Text GLabel 1500 5900 1    50   Input ~ 0
MISO
Text GLabel 1600 5900 1    50   Output ~ 0
SCK
Wire Wire Line
	1500 6450 1500 5900
Wire Wire Line
	1300 6450 1300 5900
Wire Wire Line
	1400 6450 1400 5900
Wire Wire Line
	1600 6450 1600 5900
Text GLabel 2600 6050 1    50   Input ~ 0
RESET
Text GLabel 10650 5800 2    50   Input ~ 0
MOSI
Text GLabel 9700 5700 0    50   Input ~ 0
MISO
Text GLabel 9700 5800 0    50   Input ~ 0
SCK
Text GLabel 9700 5900 0    50   Output ~ 0
RESET
Text Notes 10250 5350 2    50   ~ 0
ICSP
Wire Notes Line
	9350 5050 9350 6250
Wire Notes Line
	9350 6250 11100 6250
Text GLabel 3000 6100 1    50   Input ~ 0
RXI
Wire Wire Line
	3000 6100 3000 6450
Text GLabel 3100 6100 1    50   Output ~ 0
TXO
Wire Wire Line
	3100 6100 3100 6450
Text GLabel 9000 1700 2    50   Input ~ 0
TXO
Text GLabel 9000 1900 2    50   Output ~ 0
RXI
Text GLabel 2650 3650 0    50   UnSpc ~ 0
IN+
Text GLabel 2650 4250 0    50   UnSpc ~ 0
IN-
Wire Wire Line
	5350 4250 4950 4250
Wire Wire Line
	4950 3850 4950 4250
Wire Wire Line
	4950 3850 5050 3850
Connection ~ 4950 4250
Text GLabel 1300 3250 2    50   UnSpc ~ 0
IN+
Text GLabel 1300 3350 2    50   UnSpc ~ 0
IN-
Wire Wire Line
	1000 3250 1300 3250
Wire Wire Line
	1000 3350 1300 3350
$Comp
L power:Earth_Protective #PWR0118
U 1 1 616A9751
P 1450 3450
F 0 "#PWR0118" H 1700 3200 50  0001 C CNN
F 1 "Earth_Protective" H 1900 3300 50  0001 C CNN
F 2 "" H 1450 3350 50  0001 C CNN
F 3 "~" H 1450 3350 50  0001 C CNN
	1    1450 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 3450 1450 3450
Text GLabel 1250 3650 2    50   UnSpc ~ 0
A
Text GLabel 1250 3550 2    50   UnSpc ~ 0
B
Wire Wire Line
	1250 3550 1000 3550
Wire Wire Line
	1250 3650 1000 3650
Wire Notes Line
	650  2950 1800 2950
Wire Notes Line
	1800 2950 1800 3750
Wire Notes Line
	1800 3750 650  3750
Wire Notes Line
	650  3750 650  2950
Wire Wire Line
	8350 1300 7650 1300
$Comp
L 74xx:74HC14 U5
U 6 1 617150D4
P 6250 1500
F 0 "U5" H 6250 1817 50  0000 C CNN
F 1 "74HC14" H 6250 1726 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6250 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 6250 1500 50  0001 C CNN
	6    6250 1500
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U5
U 3 1 61716466
P 6300 2100
F 0 "U5" H 6300 2417 50  0000 C CNN
F 1 "74HC14" H 6300 2326 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6300 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 6300 2100 50  0001 C CNN
	3    6300 2100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U5
U 1 1 61717742
P 5400 1500
F 0 "U5" H 5400 1817 50  0000 C CNN
F 1 "74HC14" H 5400 1726 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5400 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 5400 1500 50  0001 C CNN
	1    5400 1500
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U5
U 2 1 61718DC8
P 3950 1500
F 0 "U5" H 3950 1183 50  0000 C CNN
F 1 "74HC14" H 3950 1274 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3950 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 3950 1500 50  0001 C CNN
	2    3950 1500
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HC14 U5
U 7 1 61720A68
P 7400 2500
F 0 "U5" H 7500 2100 50  0000 L CNN
F 1 "74HC14" H 7250 2500 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7400 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 7400 2500 50  0001 C CNN
	7    7400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1900 6750 2100
Wire Wire Line
	6750 2100 6600 2100
Wire Wire Line
	6750 1900 7950 1900
Wire Wire Line
	6550 1500 6650 1500
Text Label 6850 1500 2    50   ~ 0
TX
Text Label 6850 1900 2    50   ~ 0
RX
$Comp
L Device:R R6
U 1 1 61786D99
P 6650 1100
F 0 "R6" H 6720 1146 50  0000 L CNN
F 1 "10k" H 6720 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" V 6580 1100 50  0001 C CNN
F 3 "~" H 6650 1100 50  0001 C CNN
	1    6650 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 800  6650 950 
Wire Wire Line
	6650 1250 6650 1500
Connection ~ 6650 1500
Wire Wire Line
	6650 1500 7350 1500
$Comp
L 74xx:74HC14 U5
U 4 1 61797598
P 5400 2100
F 0 "U5" H 5400 2417 50  0000 C CNN
F 1 "74HC14" H 5400 2326 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5400 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 5400 2100 50  0001 C CNN
	4    5400 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2100 6000 2100
Wire Wire Line
	5950 1500 5750 1500
$Comp
L Device:LED D2
U 1 1 617ADB77
P 5750 2750
F 0 "D2" V 5850 2950 50  0000 R CNN
F 1 "LED_TX" H 5750 2900 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5750 2750 50  0001 C CNN
F 3 "~" H 5750 2750 50  0001 C CNN
	1    5750 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D3
U 1 1 617B2968
P 6000 2750
F 0 "D3" V 6100 2650 50  0000 R CNN
F 1 "LED RX" H 6000 2600 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6000 2750 50  0001 C CNN
F 3 "~" H 6000 2750 50  0001 C CNN
	1    6000 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 617BCE7F
P 5750 2350
F 0 "R4" H 5820 2396 50  0000 L CNN
F 1 "470" V 5750 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 2350 50  0001 C CNN
F 3 "~" H 5750 2350 50  0001 C CNN
	1    5750 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2200 5750 1500
Connection ~ 5750 1500
Wire Wire Line
	5750 1500 5700 1500
$Comp
L Device:R R5
U 1 1 617C8469
P 6000 2350
F 0 "R5" H 6070 2396 50  0000 L CNN
F 1 "470" V 6000 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 2350 50  0001 C CNN
F 3 "~" H 6000 2350 50  0001 C CNN
	1    6000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2100 6000 2200
Connection ~ 6000 2100
Wire Wire Line
	6000 2500 6000 2600
Wire Wire Line
	5750 2500 5750 2600
Wire Wire Line
	5750 2900 5750 3050
Wire Wire Line
	6000 3050 6000 2900
Connection ~ 6650 800 
$Comp
L Device:C C3
U 1 1 6181B83C
P 4350 2600
F 0 "C3" H 4465 2646 50  0000 L CNN
F 1 "0.1µF" H 4465 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4388 2450 50  0001 C CNN
F 3 "~" H 4350 2600 50  0001 C CNN
	1    4350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1500 4350 1600
Wire Wire Line
	4350 1500 4250 1500
Wire Wire Line
	4350 2750 4350 3050
Wire Wire Line
	4350 3050 5750 3050
Connection ~ 5750 3050
$Comp
L Device:R R3
U 1 1 6184557A
P 4700 1250
F 0 "R3" V 4600 1200 50  0000 C CNN
F 1 "1K" V 4700 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4630 1250 50  0001 C CNN
F 3 "~" H 4700 1250 50  0001 C CNN
	1    4700 1250
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148 D1
U 1 1 618461D2
P 4700 1600
F 0 "D1" H 4700 1383 50  0000 C CNN
F 1 "1N4148" H 4700 1474 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric" H 4700 1425 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4700 1600 50  0001 C CNN
	1    4700 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 1500 5000 1500
Wire Wire Line
	5000 1500 5000 1250
Wire Wire Line
	5000 1250 4850 1250
Wire Wire Line
	5000 1500 5000 1600
Wire Wire Line
	5000 1600 4850 1600
Connection ~ 5000 1500
Wire Wire Line
	4550 1250 4350 1250
Wire Wire Line
	4350 1250 4350 1500
Connection ~ 4350 1500
Wire Wire Line
	4550 1600 4350 1600
Connection ~ 4350 1600
Wire Wire Line
	4350 1600 4350 2450
$Comp
L Interface_UART:SN65LBC176D U4
U 1 1 618649E9
P 2750 1900
F 0 "U4" H 2750 2481 50  0000 C CNN
F 1 "SN65LBC176D" H 2750 2390 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2750 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn75lbc176.pdf" H 4350 1700 50  0001 C CNN
	1    2750 1900
	-1   0    0    -1  
$EndComp
Connection ~ 5000 1600
Wire Wire Line
	3650 1500 3300 1500
Wire Wire Line
	3300 1500 3300 1700
Wire Wire Line
	3300 2000 3050 2000
Wire Wire Line
	3050 1700 3300 1700
Connection ~ 3300 1700
Wire Wire Line
	3300 1700 3300 2000
Wire Wire Line
	3050 2100 5100 2100
Wire Wire Line
	3050 1800 5000 1800
Wire Wire Line
	5000 1600 5000 1800
Wire Wire Line
	2750 1500 2750 800 
Wire Wire Line
	2750 800  3200 800 
Wire Wire Line
	2750 2300 2750 3050
Connection ~ 4350 3050
Connection ~ 6000 3050
$Comp
L power:GND2 #PWR0120
U 1 1 618E4808
P 3200 1200
F 0 "#PWR0120" H 3200 950 50  0001 C CNN
F 1 "GND2" H 3205 1027 50  0000 C CNN
F 2 "" H 3200 1200 50  0001 C CNN
F 3 "" H 3200 1200 50  0001 C CNN
	1    3200 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 618E5145
P 3200 1000
F 0 "C2" H 3315 1046 50  0000 L CNN
F 1 "100nF" H 3315 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3238 850 50  0001 C CNN
F 3 "~" H 3200 1000 50  0001 C CNN
	1    3200 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 800  3200 850 
Connection ~ 3200 800 
Wire Wire Line
	3200 800  6650 800 
Wire Wire Line
	3200 1150 3200 1200
$Comp
L Device:C C4
U 1 1 618F7467
P 6950 2650
F 0 "C4" H 7065 2696 50  0000 L CNN
F 1 "100nF" H 6700 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6988 2500 50  0001 C CNN
F 3 "~" H 6950 2650 50  0001 C CNN
	1    6950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2200 8350 3050
Wire Wire Line
	6650 800  7050 800 
Wire Wire Line
	7650 800  7650 1300
Connection ~ 7650 800 
Wire Wire Line
	7650 800  7850 800 
Wire Wire Line
	7050 800  7050 2000
Connection ~ 7050 800 
Connection ~ 7050 2000
Wire Wire Line
	7050 2000 7400 2000
Wire Wire Line
	6950 2000 6950 2500
Wire Wire Line
	6950 2000 7050 2000
Wire Wire Line
	6950 2800 6950 3050
Connection ~ 6950 3050
Wire Wire Line
	6000 3050 6950 3050
$Comp
L Device:R R7
U 1 1 61472389
P 5950 4050
F 0 "R7" H 6020 4096 50  0000 L CNN
F 1 "698k" V 5950 3950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5880 4050 50  0001 C CNN
F 3 "~" H 5950 4050 50  0001 C CNN
	1    5950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3850 5950 3850
Wire Wire Line
	5950 3850 5950 3900
Wire Wire Line
	5950 4200 5950 4250
Connection ~ 5950 4250
$Comp
L power:+5P #PWR0105
U 1 1 614869DA
P 7850 800
F 0 "#PWR0105" H 7850 650 50  0001 C CNN
F 1 "+5P" H 7865 973 50  0000 C CNN
F 2 "" H 7850 800 50  0001 C CNN
F 3 "" H 7850 800 50  0001 C CNN
	1    7850 800 
	1    0    0    -1  
$EndComp
$Comp
L power:+5P #PWR0113
U 1 1 61492719
P 6300 3650
F 0 "#PWR0113" H 6300 3500 50  0001 C CNN
F 1 "+5P" H 6315 3823 50  0000 C CNN
F 2 "" H 6300 3650 50  0001 C CNN
F 3 "" H 6300 3650 50  0001 C CNN
	1    6300 3650
	1    0    0    -1  
$EndComp
Connection ~ 6300 3650
$Comp
L power:GNDPWR #PWR0121
U 1 1 61494E75
P 6300 4250
F 0 "#PWR0121" H 6300 4050 50  0001 C CNN
F 1 "GNDPWR" H 6304 4096 50  0000 C CNN
F 2 "" H 6300 4200 50  0001 C CNN
F 3 "" H 6300 4200 50  0001 C CNN
	1    6300 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0122
U 1 1 61496559
P 7400 3050
F 0 "#PWR0122" H 7400 2850 50  0001 C CNN
F 1 "GNDPWR" H 7404 2896 50  0000 C CNN
F 2 "" H 7400 3000 50  0001 C CNN
F 3 "" H 7400 3000 50  0001 C CNN
	1    7400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4250 6300 4250
Wire Wire Line
	6300 4100 6300 4250
Wire Wire Line
	6300 3650 6300 3900
Connection ~ 6300 4250
$Comp
L Device:CP_Small C6
U 1 1 6155B602
P 4650 3900
F 0 "C6" H 4738 3946 50  0000 L CNN
F 1 "10µF/50V" V 4500 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_4x5.4" H 4650 3900 50  0001 C CNN
F 3 "~" H 4650 3900 50  0001 C CNN
	1    4650 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C5
U 1 1 6155BEB0
P 3950 3900
F 0 "C5" H 3800 3800 50  0000 L CNN
F 1 "1µF/50V" V 4100 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_4x5.4" H 3950 3900 50  0001 C CNN
F 3 "~" H 3950 3900 50  0001 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 6155E370
P 4350 3650
F 0 "L1" V 4540 3650 50  0000 C CNN
F 1 "2.2µH/300mohm," V 4449 3650 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_MD-1616" H 4350 3650 50  0001 C CNN
F 3 "~" H 4350 3650 50  0001 C CNN
F 4 "Murata : QM18PN2R2MFRL" V 4350 3650 50  0001 C CNN "part_1"
F 5 "Taiyo Yuden : CKP1608D2R2M-T" V 4350 3650 50  0001 C CNN "paert_2"
	1    4350 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 3650 4650 3650
Wire Wire Line
	4650 3650 4650 3800
Connection ~ 4650 3650
Wire Wire Line
	4650 3650 5050 3650
Wire Wire Line
	4650 4000 4650 4250
Wire Wire Line
	4650 4250 4950 4250
Wire Wire Line
	4650 4250 3950 4250
Connection ~ 4650 4250
Wire Wire Line
	3950 4000 3950 4250
Connection ~ 3950 4250
Wire Wire Line
	3950 3800 3950 3650
Wire Wire Line
	3950 3650 4200 3650
Connection ~ 3950 3650
Wire Notes Line
	6850 3300 6850 4500
Text Notes 3400 3450 0    79   Italic 16
Input Power
$Comp
L Device:Polyfuse_Small F3
U 1 1 617D9A5D
P 2800 3650
F 0 "F3" V 2595 3650 50  0000 C CNN
F 1 "Polyfuse_Small" V 2686 3650 50  0000 C CNN
F 2 "Fuse:Fuse_1812_4532Metric" H 2850 3450 50  0001 L CNN
F 3 "~" H 2800 3650 50  0001 C CNN
	1    2800 3650
	0    1    1    0   
$EndComp
$Comp
L power:Earth_Protective #PWR0119
U 1 1 617F4B85
P 3150 4150
F 0 "#PWR0119" H 3400 3900 50  0001 C CNN
F 1 "Earth_Protective" H 3600 4000 50  0001 C CNN
F 2 "" H 3150 4050 50  0001 C CNN
F 3 "~" H 3150 4050 50  0001 C CNN
	1    3150 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Polyfuse_Small F4
U 1 1 6180011E
P 2800 4250
F 0 "F4" V 2595 4250 50  0000 C CNN
F 1 "Polyfuse_Small" V 2686 4250 50  0000 C CNN
F 2 "Fuse:Fuse_1812_4532Metric" H 2850 4050 50  0001 L CNN
F 3 "~" H 2800 4250 50  0001 C CNN
	1    2800 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 3650 3650 3750
Wire Wire Line
	3650 4050 3650 4250
Wire Wire Line
	3650 4250 3950 4250
Wire Wire Line
	3650 4250 3300 4250
Connection ~ 3650 4250
Wire Wire Line
	2650 3650 2700 3650
Wire Wire Line
	2900 3650 3000 3650
Connection ~ 3650 3650
Wire Wire Line
	3650 3650 3950 3650
Wire Wire Line
	2650 4250 2700 4250
Wire Wire Line
	2900 4250 3300 4250
Connection ~ 3300 4250
Wire Wire Line
	3000 3800 3000 3650
Connection ~ 3000 3650
Wire Wire Line
	3000 3650 3650 3650
Wire Wire Line
	3000 4100 3150 4100
Wire Wire Line
	3150 4100 3150 4150
Wire Wire Line
	3150 4100 3150 3800
Wire Wire Line
	3150 3800 3300 3800
Connection ~ 3150 4100
Wire Wire Line
	3300 4100 3300 4250
Text GLabel 950  1750 0    50   UnSpc ~ 10
B
Text GLabel 950  2350 0    50   UnSpc ~ 10
A
$Comp
L Device:Polyfuse_Small F1
U 1 1 6190BD16
P 1100 1750
F 0 "F1" V 895 1750 50  0000 C CNN
F 1 "Polyfuse_Small" V 986 1750 50  0000 C CNN
F 2 "Fuse:Fuse_1812_4532Metric" H 1150 1550 50  0001 L CNN
F 3 "~" H 1100 1750 50  0001 C CNN
	1    1100 1750
	0    1    1    0   
$EndComp
$Comp
L power:Earth_Protective #PWR0125
U 1 1 6190BD20
P 1450 2250
F 0 "#PWR0125" H 1700 2000 50  0001 C CNN
F 1 "Earth_Protective" H 1900 2100 50  0001 C CNN
F 2 "" H 1450 2150 50  0001 C CNN
F 3 "~" H 1450 2150 50  0001 C CNN
	1    1450 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D_TVS_ALT D5
U 1 1 6190BD2A
P 1600 2050
F 0 "D5" V 1554 2130 50  0000 L CNN
F 1 "28" V 1645 2130 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 1600 2050 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 1600 2050 50  0001 C CNN
F 4 "SMCJ28CA-HRA" V 1600 2050 50  0001 C CNN "Part"
	1    1600 2050
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse_Small F2
U 1 1 6190BD34
P 1100 2350
F 0 "F2" V 895 2350 50  0000 C CNN
F 1 "Polyfuse_Small" V 986 2350 50  0000 C CNN
F 2 "Fuse:Fuse_1812_4532Metric" H 1150 2150 50  0001 L CNN
F 3 "~" H 1100 2350 50  0001 C CNN
	1    1100 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 2350 1600 2350
Wire Wire Line
	950  1750 1000 1750
Wire Wire Line
	1200 1750 1300 1750
Wire Wire Line
	950  2350 1000 2350
Wire Wire Line
	1200 2350 1600 2350
Connection ~ 1600 2350
Wire Wire Line
	1300 1900 1300 1750
Connection ~ 1300 1750
Wire Wire Line
	1300 1750 1950 1750
Wire Wire Line
	1300 2200 1450 2200
Wire Wire Line
	1450 2200 1450 2250
Wire Wire Line
	1450 2200 1450 1900
Wire Wire Line
	1450 1900 1600 1900
Connection ~ 1450 2200
Wire Wire Line
	1600 2200 1600 2350
$Comp
L Device:D_TVS_ALT D6
U 1 1 619210EC
P 1950 2050
F 0 "D6" V 1904 2130 50  0000 L CNN
F 1 "6 BDGH" V 1995 2130 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 1950 2050 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 1950 2050 50  0001 C CNN
F 4 "SMCJ6.0CA-HRA" V 1950 2050 50  0001 C CNN "Part"
	1    1950 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 61921F1B
P 1950 1200
F 0 "C1" H 2065 1246 50  0000 L CNN
F 1 "1nF/50V" V 1750 1050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 1050 50  0001 C CNN
F 3 "~" H 1950 1200 50  0001 C CNN
	1    1950 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 61922CCE
P 1950 2600
F 0 "C7" H 2065 2646 50  0000 L CNN
F 1 "1nF/50V" V 1800 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 2450 50  0001 C CNN
F 3 "~" H 1950 2600 50  0001 C CNN
	1    1950 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 61923CDA
P 2350 1200
F 0 "R8" H 2420 1246 50  0000 L CNN
F 1 "1k" V 2350 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" V 2280 1200 50  0001 C CNN
F 3 "~" H 2350 1200 50  0001 C CNN
	1    2350 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 61925317
P 2350 2600
F 0 "R9" H 2420 2646 50  0000 L CNN
F 1 "10k" H 2420 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" V 2280 2600 50  0001 C CNN
F 3 "~" H 2350 2600 50  0001 C CNN
	1    2350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2200 1950 2350
Connection ~ 1950 1750
Wire Wire Line
	1950 1750 1950 1900
Wire Wire Line
	1950 1050 1950 800 
Connection ~ 2750 800 
Wire Wire Line
	2350 1050 2350 800 
Wire Wire Line
	2450 2000 2350 2000
Wire Wire Line
	2350 2000 2350 1750
Wire Wire Line
	2350 1750 1950 1750
Wire Wire Line
	2450 2100 2350 2100
Wire Wire Line
	2350 2100 2350 2350
Wire Wire Line
	2350 2350 1950 2350
Connection ~ 1950 2350
Wire Wire Line
	1950 2350 1950 2450
Wire Wire Line
	2350 2350 2350 2450
Connection ~ 2350 2350
Wire Wire Line
	1950 2750 1950 3050
Wire Wire Line
	1950 3050 2350 3050
Connection ~ 2750 3050
Wire Wire Line
	2750 3050 4350 3050
Wire Wire Line
	2350 2750 2350 3050
Connection ~ 2350 3050
Wire Wire Line
	2350 3050 2750 3050
Connection ~ 2350 800 
Wire Wire Line
	2350 800  2750 800 
Wire Wire Line
	1950 800  2350 800 
Wire Wire Line
	2350 1350 2350 1750
Connection ~ 2350 1750
Wire Wire Line
	1950 1350 1950 1750
$Comp
L Device:D_TVS_ALT D4
U 1 1 61B261BF
P 1300 2050
F 0 "D4" V 1350 1900 50  0000 L CNN
F 1 "28" V 1150 1950 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 1300 2050 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 1300 2050 50  0001 C CNN
F 4 "SMCJ28CA-HRA" V 1300 2050 50  0001 C CNN "Part"
	1    1300 2050
	0    1    1    0   
$EndComp
$Comp
L Device:D_TVS_ALT D7
U 1 1 61B5D285
P 3000 3950
F 0 "D7" V 3050 3800 50  0000 L CNN
F 1 "28" V 2850 3850 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 3000 3950 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 3000 3950 50  0001 C CNN
F 4 "SMCJ28CA-HRA" V 3000 3950 50  0001 C CNN "Part"
	1    3000 3950
	0    1    1    0   
$EndComp
$Comp
L Device:D_TVS_ALT D8
U 1 1 61B5E30F
P 3300 3950
F 0 "D8" V 3350 3800 50  0000 L CNN
F 1 "28" V 3150 3850 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 3300 3950 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 3300 3950 50  0001 C CNN
F 4 "SMCJ28CA-HRA" V 3300 3950 50  0001 C CNN "Part"
	1    3300 3950
	0    1    1    0   
$EndComp
$Comp
L Device:D_TVS_ALT D9
U 1 1 61B6016E
P 3650 3900
F 0 "D9" V 3700 3750 50  0000 L CNN
F 1 "28" V 3500 3800 50  0000 L CNN
F 2 "digikey-footprints:DO-214ACn" H 3650 3900 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_smcj_hra_datasheet.pdf.pdf" H 3650 3900 50  0001 C CNN
F 4 "SMCJ28CA-HRA" V 3650 3900 50  0001 C CNN "Part"
	1    3650 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7050 800  7650 800 
Wire Wire Line
	6950 3050 7400 3050
Wire Wire Line
	7400 3000 7400 3050
Connection ~ 7400 3050
Wire Wire Line
	7400 3050 8350 3050
Wire Notes Line
	2350 3300 2350 4500
Wire Notes Line
	2350 3300 6850 3300
Wire Notes Line
	2350 4500 6850 4500
Wire Wire Line
	5750 3050 6000 3050
$Comp
L power:GNDPWR #PWR0124
U 1 1 6150FCFE
P 7800 4000
F 0 "#PWR0124" H 7800 3800 50  0001 C CNN
F 1 "GNDPWR" H 7804 3846 50  0000 C CNN
F 2 "" H 7800 3950 50  0001 C CNN
F 3 "" H 7800 3950 50  0001 C CNN
	1    7800 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+5P #PWR0123
U 1 1 6150EBE5
P 7800 3800
F 0 "#PWR0123" H 7800 3650 50  0001 C CNN
F 1 "+5P" H 7815 3973 50  0000 C CNN
F 2 "" H 7800 3800 50  0001 C CNN
F 3 "" H 7800 3800 50  0001 C CNN
	1    7800 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 61641D1C
P 9000 3700
F 0 "#PWR0117" H 9000 3550 50  0001 C CNN
F 1 "+5V" H 9015 3873 50  0000 C CNN
F 2 "" H 9000 3700 50  0001 C CNN
F 3 "" H 9000 3700 50  0001 C CNN
	1    9000 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 61640F80
P 9000 4200
F 0 "#PWR0116" H 9000 3950 50  0001 C CNN
F 1 "GND" H 9005 4027 50  0000 C CNN
F 2 "" H 9000 4200 50  0001 C CNN
F 3 "" H 9000 4200 50  0001 C CNN
	1    9000 4200
	1    0    0    -1  
$EndComp
$Comp
L Converter_DCDC:MEE1S0303SC PS1
U 1 1 6146C04E
P 8450 3900
F 0 "PS1" H 8300 4250 50  0000 C CNN
F 1 "MEE1S0505SC" H 8150 3400 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MEE1SxxxxSC_THT" H 7400 3650 50  0001 L CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mee1.pdf" H 9500 3600 50  0001 L CNN
	1    8450 3900
	1    0    0    -1  
$EndComp
Wire Notes Line width 20 style dash_dot rgb(185, 20, 194)
	8450 850  8450 4600
Wire Notes Line width 20 rgb(154, 15, 194)
	550  4600 550  550 
Wire Notes Line width 20 style dash_dot rgb(134, 16, 194)
	550  550  8450 550 
Wire Notes Line width 20 rgb(126, 15, 194)
	8450 550  8450 850 
Wire Notes Line width 20 style dash_dot rgb(139, 16, 194)
	550  4600 8450 4600
Wire Notes Line
	11100 3200 9600 3200
Wire Notes Line
	9600 3200 9600 4500
Wire Wire Line
	9000 4000 9000 4200
Wire Wire Line
	9000 3700 9000 3800
Wire Wire Line
	8850 3800 9000 3800
Wire Wire Line
	8850 4000 9000 4000
Wire Wire Line
	7800 3800 8050 3800
Wire Wire Line
	7800 4000 8050 4000
Wire Wire Line
	8550 1300 9000 1300
Wire Wire Line
	9000 1300 9000 1150
Wire Wire Line
	8550 2300 9000 2300
Wire Wire Line
	9000 2300 9000 2500
Text Notes 800  950  0    118  Italic 24
RS485
Text Notes 850  4400 0    118  Italic 24
Power
$Comp
L dk_PMIC-Voltage-Regulators-Linear:LP2985-33DBVR U6
U 1 1 61F32695
P 6850 5300
F 0 "U6" H 6650 5550 60  0000 C CNN
F 1 "LP2985-33DBVR" H 7350 5650 60  0000 C CNN
F 2 "digikey-footprints:SOT-753" H 7050 5500 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flp2985" H 7050 5600 60  0001 L CNN
F 4 "296-18476-1-ND" H 7050 5700 60  0001 L CNN "Digi-Key_PN"
F 5 "LP2985-33DBVR" H 7050 5800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7050 5900 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 7050 6000 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flp2985" H 7050 6100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LP2985-33DBVR/296-18476-1-ND/809911" H 7050 6200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 150MA SOT23-5" H 7050 6300 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7050 6400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7050 6500 60  0001 L CNN "Status"
	1    6850 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 61F3426F
P 8500 5200
F 0 "R11" H 8550 5150 50  0000 L CNN
F 1 "10K" V 8400 5150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8500 5200 50  0001 C CNN
F 3 "~" H 8500 5200 50  0001 C CNN
	1    8500 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 61F34DC3
P 8000 5800
F 0 "R10" H 8059 5846 50  0000 L CNN
F 1 "1K" H 8059 5755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8000 5800 50  0001 C CNN
F 3 "~" H 8000 5800 50  0001 C CNN
	1    8000 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 61F35A17
P 8250 5400
F 0 "C9" V 8350 5250 50  0000 L CNN
F 1 "0.1µF" V 8150 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8288 5250 50  0001 C CNN
F 3 "~" H 8250 5400 50  0001 C CNN
	1    8250 5400
	0    1    1    0   
$EndComp
$Comp
L dk_Tactile-Switches:B3U-1000P S1
U 1 1 61F60CC9
P 8500 5700
F 0 "S1" V 8447 5838 60  0000 L CNN
F 1 "B3U-1000P" V 8553 5838 60  0000 L CNN
F 2 "digikey-footprints:Switch_Tactile_SMD_B3U-1000P" H 8700 5900 60  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3u.pdf" H 8700 6000 60  0001 L CNN
F 4 "SW1020CT-ND" H 8700 6100 60  0001 L CNN "Digi-Key_PN"
F 5 "B3U-1000P" H 8700 6200 60  0001 L CNN "MPN"
F 6 "Switches" H 8700 6300 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 8700 6400 60  0001 L CNN "Family"
F 8 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3u.pdf" H 8700 6500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/omron-electronics-inc-emc-div/B3U-1000P/SW1020CT-ND/1534357" H 8700 6600 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 12V" H 8700 6700 60  0001 L CNN "Description"
F 11 "Omron Electronics Inc-EMC Div" H 8700 6800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8700 6900 60  0001 L CNN "Status"
	1    8500 5700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 61F6DF1C
P 8350 6000
F 0 "#PWR0126" H 8350 5750 50  0001 C CNN
F 1 "GND" H 8355 5827 50  0000 C CNN
F 2 "" H 8350 6000 50  0001 C CNN
F 3 "" H 8350 6000 50  0001 C CNN
	1    8350 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5900 8500 5950
Wire Wire Line
	8500 5950 8350 5950
Wire Wire Line
	8350 5950 8350 6000
Wire Wire Line
	8350 5950 8000 5950
Wire Wire Line
	8000 5950 8000 5900
Connection ~ 8350 5950
Wire Wire Line
	8500 5300 8500 5400
Wire Wire Line
	8500 5400 8400 5400
Connection ~ 8500 5400
Wire Wire Line
	8500 5400 8500 5500
Wire Wire Line
	8100 5400 8000 5400
Wire Wire Line
	8000 5400 8000 5700
$Comp
L power:+3.3V #PWR0127
U 1 1 61FD3B0B
P 8500 5000
F 0 "#PWR0127" H 8500 4850 50  0001 C CNN
F 1 "+3.3V" H 8515 5173 50  0000 C CNN
F 2 "" H 8500 5000 50  0001 C CNN
F 3 "" H 8500 5000 50  0001 C CNN
	1    8500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5100 8500 5050
Text GLabel 8850 5400 2    50   Output ~ 0
RESET
$Comp
L Diode:1N4148 D10
U 1 1 61FF8761
P 8800 5200
F 0 "D10" V 8700 5100 50  0000 C CNN
F 1 "1N4148" H 8750 5300 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric" H 8800 5025 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8800 5200 50  0001 C CNN
	1    8800 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	8800 5350 8800 5400
Wire Wire Line
	8500 5400 8800 5400
Wire Wire Line
	8800 5400 8850 5400
Connection ~ 8800 5400
Wire Wire Line
	8800 5050 8500 5050
Connection ~ 8500 5050
Wire Wire Line
	8500 5050 8500 5000
Wire Notes Line
	9250 4700 9250 6250
Wire Notes Line
	9250 6250 7850 6250
Wire Notes Line
	7850 6250 7850 4700
Wire Notes Line
	7850 4700 9250 4700
Text Notes 7950 4900 0    50   Italic 10
RESET
$Comp
L Device:Resonator_Small Y1
U 1 1 6205B314
P 1800 6250
F 0 "Y1" H 1750 6000 50  0000 R CNN
F 1 "8M" H 1800 5900 50  0000 R CNN
F 2 "Crystal:Resonator_SMD_muRata_CSTxExxV-3Pin_3.0x1.1mm_HandSoldering" H 1775 6250 50  0001 C CNN
F 3 "~" H 1775 6250 50  0001 C CNN
	1    1800 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 6350 1700 6450
$Comp
L power:GND #PWR0128
U 1 1 6209B276
P 1800 5850
F 0 "#PWR0128" H 1800 5600 50  0001 C CNN
F 1 "GND" H 1805 5677 50  0000 C CNN
F 2 "" H 1800 5850 50  0001 C CNN
F 3 "" H 1800 5850 50  0001 C CNN
	1    1800 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 5850 1800 6050
Wire Wire Line
	800  6000 800  6800
Text Label 10600 4600 0    50   ~ 0
RxD
Text Label 10600 4700 0    50   ~ 0
TxD
Text GLabel 10550 4700 0    50   Input ~ 0
TxD
Text GLabel 10550 4600 0    50   Output ~ 0
RxD
Wire Wire Line
	10550 4600 10800 4600
Wire Wire Line
	10550 4700 10800 4700
$Comp
L power:GND #PWR0129
U 1 1 620FC491
P 10600 4800
F 0 "#PWR0129" H 10600 4550 50  0001 C CNN
F 1 "GND" V 10605 4627 50  0000 C CNN
F 2 "" H 10600 4800 50  0001 C CNN
F 3 "" H 10600 4800 50  0001 C CNN
	1    10600 4800
	0    1    1    0   
$EndComp
Text Label 10750 4800 2    50   ~ 0
GND
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 6215B29F
P 11000 4700
F 0 "J7" H 10850 4550 50  0000 L CNN
F 1 "debug" H 10700 4450 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 11000 4700 50  0001 C CNN
F 3 "~" H 11000 4700 50  0001 C CNN
	1    11000 4700
	1    0    0    -1  
$EndComp
Wire Notes Line
	10300 4500 10300 5050
Wire Notes Line
	9350 5050 11100 5050
Wire Notes Line
	11100 3200 11100 6250
$Comp
L power:GND #PWR0130
U 1 1 6220B5CE
P 10050 4200
F 0 "#PWR0130" H 10050 3950 50  0001 C CNN
F 1 "GND" H 10055 4027 50  0000 C CNN
F 2 "" H 10050 4200 50  0001 C CNN
F 3 "" H 10050 4200 50  0001 C CNN
	1    10050 4200
	1    0    0    -1  
$EndComp
Connection ~ 10050 4200
Wire Wire Line
	10050 4200 9850 4200
Text GLabel 9900 4600 0    50   BiDi ~ 0
SDA
Text GLabel 9900 4700 0    50   Input ~ 0
SCL
Text Label 10050 4600 2    50   ~ 0
SDA
Text Label 10050 4700 2    50   ~ 0
SCL
$Comp
L power:+3.3V #PWR0131
U 1 1 622316B3
P 9850 4800
F 0 "#PWR0131" H 9850 4650 50  0001 C CNN
F 1 "+3.3V" V 9850 5050 50  0000 C CNN
F 2 "" H 9850 4800 50  0001 C CNN
F 3 "" H 9850 4800 50  0001 C CNN
	1    9850 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0132
U 1 1 62232FFB
P 9850 4900
F 0 "#PWR0132" H 9850 4650 50  0001 C CNN
F 1 "GND" V 9855 4772 50  0000 R CNN
F 2 "" H 9850 4900 50  0001 C CNN
F 3 "" H 9850 4900 50  0001 C CNN
	1    9850 4900
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 622341E5
P 10250 4800
F 0 "J6" H 10100 4950 50  0000 L CNN
F 1 "oled" H 9900 4950 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 10250 4800 50  0001 C CNN
F 3 "~" H 10250 4800 50  0001 C CNN
	1    10250 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	9900 4600 10050 4600
Wire Wire Line
	10050 4700 9900 4700
Wire Wire Line
	9850 4900 10050 4900
Wire Wire Line
	9850 4800 10050 4800
Wire Notes Line
	9400 4500 9400 5050
Wire Notes Line
	9400 4500 11100 4500
Wire Wire Line
	10600 4800 10800 4800
Text Label 1800 1750 0    50   Italic 10
BF
Text Label 1850 2350 0    50   Italic 10
AF
Text Label 3350 3650 0    50   Italic 10
FI+
Text Label 3450 4250 0    50   Italic 10
FI-
Text Label 4850 3650 0    50   Italic 10
FIF+
$Comp
L Device:CP_Small C8
U 1 1 6240CDB1
P 6300 4000
F 0 "C8" H 6388 4046 50  0000 L CNN
F 1 "10µF/16V" V 6200 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6300 4000 50  0001 C CNN
F 3 "~" H 6300 4000 50  0001 C CNN
	1    6300 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0133
U 1 1 625106BB
P 6950 4950
F 0 "#PWR0133" H 6950 4800 50  0001 C CNN
F 1 "+5V" H 6965 5123 50  0000 C CNN
F 2 "" H 6950 4950 50  0001 C CNN
F 3 "" H 6950 4950 50  0001 C CNN
	1    6950 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4950 6950 5000
Wire Wire Line
	6450 4950 6450 5300
Wire Wire Line
	6850 5600 6850 5650
$Comp
L power:GND #PWR0134
U 1 1 6254D459
P 7500 5650
F 0 "#PWR0134" H 7500 5400 50  0001 C CNN
F 1 "GND" H 7505 5477 50  0000 C CNN
F 2 "" H 7500 5650 50  0001 C CNN
F 3 "" H 7500 5650 50  0001 C CNN
	1    7500 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5600 7500 5650
Wire Wire Line
	7500 5400 7500 5300
Wire Wire Line
	7500 5300 7250 5300
$Comp
L Device:CP_Small C12
U 1 1 62574639
P 6350 5450
F 0 "C12" H 6438 5496 50  0000 L CNN
F 1 "10µF/16V" V 6250 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6350 5450 50  0001 C CNN
F 3 "~" H 6350 5450 50  0001 C CNN
	1    6350 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 5300 6350 5300
Wire Wire Line
	6350 5300 6350 5350
Connection ~ 6450 5300
$Comp
L power:GND #PWR0135
U 1 1 62589B87
P 6350 5650
F 0 "#PWR0135" H 6350 5400 50  0001 C CNN
F 1 "GND" H 6355 5477 50  0000 C CNN
F 2 "" H 6350 5650 50  0001 C CNN
F 3 "" H 6350 5650 50  0001 C CNN
	1    6350 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5550 6350 5650
$Comp
L Device:CP_Small C13
U 1 1 6259E2CB
P 7500 5500
F 0 "C13" H 7588 5546 50  0000 L CNN
F 1 "10µF/16V" V 7400 5350 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7500 5500 50  0001 C CNN
F 3 "~" H 7500 5500 50  0001 C CNN
	1    7500 5500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0136
U 1 1 625A3E56
P 7500 5250
F 0 "#PWR0136" H 7500 5100 50  0001 C CNN
F 1 "+3.3V" H 7515 5423 50  0000 C CNN
F 2 "" H 7500 5250 50  0001 C CNN
F 3 "" H 7500 5250 50  0001 C CNN
	1    7500 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5250 7500 5300
Connection ~ 7500 5300
Wire Wire Line
	6950 2600 6950 2500
Connection ~ 6950 2500
$Comp
L Device:C C10
U 1 1 625CF403
P 800 7300
F 0 "C10" H 915 7346 50  0000 L CNN
F 1 "100nF" H 550 7400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 838 7150 50  0001 C CNN
F 3 "~" H 800 7300 50  0001 C CNN
	1    800  7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 625D06FC
P 800 7500
F 0 "#PWR0137" H 800 7250 50  0001 C CNN
F 1 "GND" H 805 7327 50  0000 C CNN
F 2 "" H 800 7500 50  0001 C CNN
F 3 "" H 800 7500 50  0001 C CNN
	1    800  7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  7450 800  7500
$Comp
L Device:C C11
U 1 1 625FAFC7
P 4200 7300
F 0 "C11" H 4315 7346 50  0000 L CNN
F 1 "100nF" H 3950 7400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4238 7150 50  0001 C CNN
F 3 "~" H 4200 7300 50  0001 C CNN
	1    4200 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 6350 1900 6450
Wire Wire Line
	4200 7450 4050 7450
Wire Wire Line
	4050 7450 4050 7500
Wire Wire Line
	4050 7450 4000 7450
Wire Wire Line
	4000 7450 4000 7300
Wire Wire Line
	4000 7100 3900 7100
Connection ~ 4050 7450
Wire Wire Line
	3900 7200 4000 7200
Connection ~ 4000 7200
Wire Wire Line
	4000 7200 4000 7100
Wire Wire Line
	3900 7300 4000 7300
Connection ~ 4000 7300
Wire Wire Line
	4000 7300 4000 7200
Wire Wire Line
	4200 7150 4200 6800
Wire Wire Line
	4200 6800 3900 6800
Wire Wire Line
	800  6800 900  6800
Connection ~ 800  6800
Wire Wire Line
	800  6800 800  6900
Wire Wire Line
	800  6900 900  6900
Connection ~ 800  6900
Wire Wire Line
	800  6900 800  7000
Wire Wire Line
	900  7000 800  7000
Connection ~ 800  7000
Wire Wire Line
	800  7000 800  7150
Wire Wire Line
	7200 6350 7100 6350
Wire Wire Line
	7100 6350 7100 6150
Wire Wire Line
	7100 6150 7200 6150
Connection ~ 7100 6150
Wire Wire Line
	7400 6150 7450 6150
Wire Wire Line
	7400 6350 7450 6350
Text Notes 7600 6050 0    50   Italic 10
IIC
Wire Notes Line
	6100 4700 7800 4700
Wire Notes Line
	7800 4700 7800 5900
Wire Notes Line
	7800 5900 6100 5900
Wire Notes Line
	6100 5900 6100 4700
Text Notes 6100 4800 0    50   Italic 10
3V3
Wire Wire Line
	6950 4950 6450 4950
Connection ~ 6950 4950
Wire Notes Line
	6950 5950 7800 5950
Wire Notes Line
	7800 5950 7800 6450
Wire Notes Line
	7800 6450 6950 6450
Wire Notes Line
	6950 6450 6950 5950
$Comp
L Connector:Conn_01x08_Male J13
U 1 1 62AB6F92
P 6400 7150
F 0 "J13" H 6372 7124 50  0000 R CNN
F 1 "JST-p1" H 6700 7650 50  0000 R CNN
F 2 "Connector_JST:JST_SH_BM08B-SRSS-TB_1x08-1MP_P1.00mm_Vertical" H 6400 7150 50  0001 C CNN
F 3 "~" H 6400 7150 50  0001 C CNN
	1    6400 7150
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0138
U 1 1 62AB9000
P 5900 6750
F 0 "#PWR0138" H 5900 6600 50  0001 C CNN
F 1 "+5V" H 5915 6923 50  0000 C CNN
F 2 "" H 5900 6750 50  0001 C CNN
F 3 "" H 5900 6750 50  0001 C CNN
	1    5900 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0139
U 1 1 62AD1175
P 5900 6950
F 0 "#PWR0139" H 5900 6700 50  0001 C CNN
F 1 "GND" V 5905 6822 50  0000 R CNN
F 2 "" H 5900 6950 50  0001 C CNN
F 3 "" H 5900 6950 50  0001 C CNN
	1    5900 6950
	0    1    1    0   
$EndComp
Text GLabel 3700 5950 1    50   Output ~ 0
D7
Text GLabel 3600 5950 1    50   Output ~ 0
D6
Wire Wire Line
	3600 5950 3600 6450
Wire Wire Line
	3700 5950 3700 6450
Text GLabel 5900 7050 0    50   Input ~ 0
D6
Text GLabel 5900 7350 0    50   Input ~ 0
D7
Text GLabel 5900 7250 0    50   Input ~ 0
TxD
Text GLabel 5900 7150 0    50   Output ~ 0
RxD
Wire Wire Line
	5900 6750 5900 6850
Wire Wire Line
	5900 6850 6200 6850
Wire Wire Line
	5900 6950 6200 6950
Wire Wire Line
	5900 7050 6200 7050
Wire Wire Line
	5900 7150 6200 7150
Wire Wire Line
	5900 7250 6200 7250
Wire Wire Line
	5900 7350 6200 7350
Text Label 5950 7050 0    50   ~ 0
D_Set
Text Label 5900 7350 0    50   ~ 0
D_RESET
$Comp
L Connector:Conn_01x08_Male J10
U 1 1 62C62CF6
P 5500 7250
F 0 "J10" H 5472 7224 50  0000 R CNN
F 1 "JST-p2" H 5800 6700 50  0000 R CNN
F 2 "Connector_JST:JST_SH_BM08B-SRSS-TB_1x08-1MP_P1.00mm_Vertical" H 5500 7250 50  0001 C CNN
F 3 "~" H 5500 7250 50  0001 C CNN
	1    5500 7250
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0140
U 1 1 62C62D00
P 5000 6750
F 0 "#PWR0140" H 5000 6600 50  0001 C CNN
F 1 "+5V" H 5015 6923 50  0000 C CNN
F 2 "" H 5000 6750 50  0001 C CNN
F 3 "" H 5000 6750 50  0001 C CNN
	1    5000 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 62C62D0A
P 5000 6950
F 0 "#PWR0141" H 5000 6700 50  0001 C CNN
F 1 "GND" V 5005 6822 50  0000 R CNN
F 2 "" H 5000 6950 50  0001 C CNN
F 3 "" H 5000 6950 50  0001 C CNN
	1    5000 6950
	0    1    1    0   
$EndComp
Text GLabel 5000 7050 0    50   Input ~ 0
D6
Text GLabel 5000 7350 0    50   Input ~ 0
D7
Text GLabel 5000 7250 0    50   Input ~ 0
TxD
Text GLabel 5000 7150 0    50   Output ~ 0
RxD
Wire Wire Line
	5000 6750 5000 6850
Wire Wire Line
	5000 6850 5300 6850
Wire Wire Line
	5000 6950 5300 6950
Wire Wire Line
	5000 7050 5300 7050
Wire Wire Line
	5000 7150 5300 7150
Wire Wire Line
	5000 7250 5300 7250
Wire Wire Line
	5000 7350 5300 7350
Text Label 5050 7050 0    50   ~ 0
D_Set
Text Label 5000 7350 0    50   ~ 0
D_RESET
Wire Notes Line
	4650 6450 6600 6450
Wire Notes Line
	6600 6450 6600 7700
Wire Notes Line
	6600 7700 4650 7700
Wire Notes Line
	4650 7700 4650 6450
Text Notes 5450 6600 0    50   Italic 10
PM2.5
$Comp
L Connector:Screw_Terminal_01x03 J8
U 1 1 62E56132
P 1800 5100
F 0 "J8" H 1880 5142 50  0000 L CNN
F 1 "1-wire" H 1880 5051 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal" H 1800 5100 50  0001 C CNN
F 3 "~" H 1800 5100 50  0001 C CNN
	1    1800 5100
	1    0    0    -1  
$EndComp
Text GLabel 1000 5100 0    50   Output ~ 0
INT1
$Comp
L power:+3.3V #PWR0142
U 1 1 62E5613D
P 1200 5000
F 0 "#PWR0142" H 1200 4850 50  0001 C CNN
F 1 "+3.3V" H 1215 5173 50  0000 C CNN
F 2 "" H 1200 5000 50  0001 C CNN
F 3 "" H 1200 5000 50  0001 C CNN
	1    1200 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5000 1500 5000
Wire Wire Line
	1600 5100 1550 5100
Wire Wire Line
	1500 5200 1550 5200
Wire Wire Line
	1000 5100 1200 5100
$Comp
L power:GND #PWR0143
U 1 1 62E5614B
P 1550 5200
F 0 "#PWR0143" H 1550 4950 50  0001 C CNN
F 1 "GND" H 1555 5027 50  0000 C CNN
F 2 "" H 1550 5200 50  0001 C CNN
F 3 "" H 1550 5200 50  0001 C CNN
	1    1550 5200
	1    0    0    -1  
$EndComp
Connection ~ 1550 5200
Wire Wire Line
	1550 5200 1600 5200
Wire Wire Line
	1200 5100 1200 5200
Connection ~ 1200 5100
Wire Wire Line
	1200 5100 1250 5100
$Comp
L Device:R R13
U 1 1 62E5615A
P 1400 5100
F 0 "R13" V 1350 5250 50  0000 C CNN
F 1 "100" V 1400 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1330 5100 50  0001 C CNN
F 3 "~" H 1400 5100 50  0001 C CNN
	1    1400 5100
	0    1    1    0   
$EndComp
$Comp
L Diode:BZT52Bxx D11
U 1 1 62E56164
P 1350 5200
F 0 "D11" H 1450 5150 50  0000 C CNN
F 1 "BZT52Bxx" H 1200 5100 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1350 5025 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzt52b2v4.pdf" H 1350 5200 50  0001 C CNN
	1    1350 5200
	1    0    0    -1  
$EndComp
Text GLabel 2000 6150 1    50   UnSpc ~ 0
AD0
Wire Wire Line
	2000 6150 2000 6450
Text GLabel 2100 6150 1    50   UnSpc ~ 0
AD1
Wire Wire Line
	2100 6150 2100 6450
Text Label 2100 6350 1    50   ~ 0
A1
Text Label 2000 6350 1    50   ~ 0
A0
Wire Notes Line
	4650 5500 6000 5500
Wire Notes Line
	6000 6300 4650 6300
$Comp
L Connector:Screw_Terminal_01x03 J11
U 1 1 62FC8A1B
P 5700 5050
F 0 "J11" H 5780 5092 50  0000 L CNN
F 1 "AI0" H 5780 5001 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal" H 5700 5050 50  0001 C CNN
F 3 "~" H 5700 5050 50  0001 C CNN
	1    5700 5050
	1    0    0    -1  
$EndComp
Text GLabel 4900 5050 0    50   UnSpc ~ 0
AD0
Wire Wire Line
	5500 4950 5400 4950
Wire Wire Line
	5500 5050 5450 5050
Wire Wire Line
	5400 5150 5450 5150
Wire Wire Line
	4900 5050 5100 5050
$Comp
L power:GND #PWR0144
U 1 1 62FC8A2A
P 5550 5250
F 0 "#PWR0144" H 5550 5000 50  0001 C CNN
F 1 "GND" H 5555 5077 50  0000 C CNN
F 2 "" H 5550 5250 50  0001 C CNN
F 3 "" H 5550 5250 50  0001 C CNN
	1    5550 5250
	1    0    0    -1  
$EndComp
Connection ~ 5450 5150
Wire Wire Line
	5450 5150 5500 5150
Wire Wire Line
	5100 5050 5100 5150
Connection ~ 5100 5050
Wire Wire Line
	5100 5050 5150 5050
$Comp
L Device:R R19
U 1 1 62FC8A39
P 5300 5050
F 0 "R19" V 5250 5200 50  0000 C CNN
F 1 "1K" V 5300 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 5050 50  0001 C CNN
F 3 "~" H 5300 5050 50  0001 C CNN
	1    5300 5050
	0    1    1    0   
$EndComp
$Comp
L Diode:BZT52Bxx D13
U 1 1 62FC8A43
P 5250 5150
F 0 "D13" H 5000 5150 50  0000 C CNN
F 1 "BZT52B3V3" H 4850 5400 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 5250 4975 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzt52b2v4.pdf" H 5250 5150 50  0001 C CNN
	1    5250 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0145
U 1 1 62FC8A4D
P 5100 4950
F 0 "#PWR0145" H 5100 4800 50  0001 C CNN
F 1 "+5V" H 5115 5123 50  0000 C CNN
F 2 "" H 5100 4950 50  0001 C CNN
F 3 "" H 5100 4950 50  0001 C CNN
	1    5100 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 62FC8A57
P 5300 5250
F 0 "R20" V 5300 4950 50  0000 C CNN
F 1 "2K2" V 5300 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 5250 50  0001 C CNN
F 3 "~" H 5300 5250 50  0001 C CNN
	1    5300 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 5150 5450 5250
Wire Wire Line
	5450 5250 5550 5250
Connection ~ 5450 5250
Wire Wire Line
	5150 5250 5100 5250
Wire Wire Line
	5100 5250 5100 5150
Connection ~ 5100 5150
Wire Notes Line
	4650 4700 6000 4700
Text Notes 5500 4800 0    50   Italic 10
Analog 0
Wire Notes Line
	4650 4700 4650 6300
Wire Notes Line
	6000 4700 6000 6300
$Comp
L Device:R_Small R18
U 1 1 62FF0153
P 5300 4950
F 0 "R18" V 5250 5050 50  0000 L CNN
F 1 "10" V 5300 4900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5300 4950 50  0001 C CNN
F 3 "~" H 5300 4950 50  0001 C CNN
	1    5300 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 4950 5100 4950
$Comp
L Connector:Screw_Terminal_01x03 J12
U 1 1 630119F9
P 5750 5850
F 0 "J12" H 5830 5892 50  0000 L CNN
F 1 "AI0" H 5830 5801 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal" H 5750 5850 50  0001 C CNN
F 3 "~" H 5750 5850 50  0001 C CNN
	1    5750 5850
	1    0    0    -1  
$EndComp
Text GLabel 4950 5850 0    50   UnSpc ~ 0
AD1
Wire Wire Line
	5550 5750 5450 5750
Wire Wire Line
	5550 5850 5500 5850
Wire Wire Line
	5450 5950 5500 5950
Wire Wire Line
	4950 5850 5150 5850
$Comp
L power:GND #PWR0146
U 1 1 63011A08
P 5600 6050
F 0 "#PWR0146" H 5600 5800 50  0001 C CNN
F 1 "GND" H 5605 5877 50  0000 C CNN
F 2 "" H 5600 6050 50  0001 C CNN
F 3 "" H 5600 6050 50  0001 C CNN
	1    5600 6050
	1    0    0    -1  
$EndComp
Connection ~ 5500 5950
Wire Wire Line
	5500 5950 5550 5950
Wire Wire Line
	5150 5850 5150 5950
Connection ~ 5150 5850
Wire Wire Line
	5150 5850 5200 5850
$Comp
L Device:R R22
U 1 1 63011A17
P 5350 5850
F 0 "R22" V 5300 6000 50  0000 C CNN
F 1 "1K" V 5350 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5280 5850 50  0001 C CNN
F 3 "~" H 5350 5850 50  0001 C CNN
	1    5350 5850
	0    1    1    0   
$EndComp
$Comp
L Diode:BZT52Bxx D14
U 1 1 63011A21
P 5300 5950
F 0 "D14" H 5050 5950 50  0000 C CNN
F 1 "BZT52B3V3" H 4900 6200 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 5300 5775 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzt52b2v4.pdf" H 5300 5950 50  0001 C CNN
	1    5300 5950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0147
U 1 1 63011A2B
P 5150 5750
F 0 "#PWR0147" H 5150 5600 50  0001 C CNN
F 1 "+5V" H 5165 5923 50  0000 C CNN
F 2 "" H 5150 5750 50  0001 C CNN
F 3 "" H 5150 5750 50  0001 C CNN
	1    5150 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 63011A35
P 5350 6050
F 0 "R23" V 5350 5750 50  0000 C CNN
F 1 "2K2" V 5350 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5280 6050 50  0001 C CNN
F 3 "~" H 5350 6050 50  0001 C CNN
	1    5350 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 5950 5500 6050
Wire Wire Line
	5500 6050 5600 6050
Connection ~ 5500 6050
Wire Wire Line
	5200 6050 5150 6050
Wire Wire Line
	5150 6050 5150 5950
Connection ~ 5150 5950
Text Notes 5550 5600 0    50   Italic 10
Analog 1
$Comp
L Device:R_Small R21
U 1 1 63011A46
P 5350 5750
F 0 "R21" V 5300 5850 50  0000 L CNN
F 1 "10" V 5350 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5350 5750 50  0001 C CNN
F 3 "~" H 5350 5750 50  0001 C CNN
	1    5350 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 5750 5150 5750
$Comp
L Device:R_Small R12
U 1 1 63035A68
P 1400 5000
F 0 "R12" V 1350 5100 50  0000 L CNN
F 1 "10" V 1400 4950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1400 5000 50  0001 C CNN
F 3 "~" H 1400 5000 50  0001 C CNN
	1    1400 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 5000 1200 5000
Connection ~ 3300 6450
Wire Wire Line
	3300 6450 3300 6050
Wire Notes Line
	750  4700 2300 4700
Wire Notes Line
	2300 4700 2300 5500
Wire Notes Line
	2300 5500 750  5500
Wire Notes Line
	750  5500 750  4700
$Comp
L dk_Transistors-FETs-MOSFETs-Single:2N7002 Q1
U 1 1 6317DDDF
P 3250 5400
F 0 "Q1" V 3411 5400 60  0000 C CNN
F 1 "2N7002" V 3517 5400 60  0000 C CNN
F 2 "digikey-footprints:SOT-23-3" H 3450 5600 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 3450 5700 60  0001 L CNN
F 4 "2N7002NCT-ND" H 3450 5800 60  0001 L CNN "Digi-Key_PN"
F 5 "2N7002" H 3450 5900 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3450 6000 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 3450 6100 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 3450 6200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/2N7002/2N7002NCT-ND/244664" H 3450 6300 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 115MA SOT-23" H 3450 6400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 3450 6500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3450 6600 60  0001 L CNN "Status"
	1    3250 5400
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J9
U 1 1 631A5568
P 4150 5400
F 0 "J9" H 4230 5442 50  0000 L CNN
F 1 "1-wire" H 4230 5351 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal" H 4150 5400 50  0001 C CNN
F 3 "~" H 4150 5400 50  0001 C CNN
	1    4150 5400
	1    0    0    -1  
$EndComp
Text GLabel 2850 5400 0    50   Output ~ 0
INT0
$Comp
L power:+3.3V #PWR0148
U 1 1 631A5573
P 2900 4950
F 0 "#PWR0148" H 2900 4800 50  0001 C CNN
F 1 "+3.3V" H 2915 5123 50  0000 C CNN
F 2 "" H 2900 4950 50  0001 C CNN
F 3 "" H 2900 4950 50  0001 C CNN
	1    2900 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5300 3850 5300
Wire Wire Line
	3950 5400 3900 5400
$Comp
L power:GND #PWR0149
U 1 1 631A5581
P 3950 5500
F 0 "#PWR0149" H 3950 5250 50  0001 C CNN
F 1 "GND" H 3955 5327 50  0000 C CNN
F 2 "" H 3950 5500 50  0001 C CNN
F 3 "" H 3950 5500 50  0001 C CNN
	1    3950 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 5400 3550 5500
Wire Wire Line
	3550 5400 3600 5400
$Comp
L Device:R R16
U 1 1 631A5590
P 3750 5400
F 0 "R16" V 3700 5550 50  0000 C CNN
F 1 "100" V 3750 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 5400 50  0001 C CNN
F 3 "~" H 3750 5400 50  0001 C CNN
	1    3750 5400
	0    1    1    0   
$EndComp
$Comp
L Diode:BZT52Bxx D12
U 1 1 631A559A
P 3700 5500
F 0 "D12" H 3800 5450 50  0000 C CNN
F 1 "BZT52Bxx" H 3650 5350 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 3700 5325 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzt52b2v4.pdf" H 3700 5500 50  0001 C CNN
	1    3700 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R17
U 1 1 631A55A4
P 3850 5150
F 0 "R17" H 3700 5050 50  0000 L CNN
F 1 "10" V 3850 5100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3850 5150 50  0001 C CNN
F 3 "~" H 3850 5150 50  0001 C CNN
	1    3850 5150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0150
U 1 1 631EBBDA
P 3550 4950
F 0 "#PWR0150" H 3550 4800 50  0001 C CNN
F 1 "+5V" H 3565 5123 50  0000 C CNN
F 2 "" H 3550 4950 50  0001 C CNN
F 3 "" H 3550 4950 50  0001 C CNN
	1    3550 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 631F06E6
P 3550 5150
F 0 "R15" V 3500 5300 50  0000 C CNN
F 1 "10K" V 3550 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 5150 50  0001 C CNN
F 3 "~" H 3550 5150 50  0001 C CNN
	1    3550 5150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R14
U 1 1 631F1CC3
P 2900 5150
F 0 "R14" V 2850 5300 50  0000 C CNN
F 1 "10K" V 2900 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2830 5150 50  0001 C CNN
F 3 "~" H 2900 5150 50  0001 C CNN
	1    2900 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3450 5400 3550 5400
Connection ~ 3550 5400
Wire Wire Line
	3550 5300 3550 5400
Wire Wire Line
	3550 4950 3550 5000
Wire Wire Line
	3850 5250 3850 5300
Wire Wire Line
	3850 4950 3550 4950
Wire Wire Line
	3850 4950 3850 5050
Connection ~ 3550 4950
Wire Wire Line
	2900 4950 2900 5000
Wire Wire Line
	2900 5300 2900 5400
Wire Wire Line
	2900 5400 3050 5400
Wire Wire Line
	2850 5400 2900 5400
Connection ~ 2900 5400
Wire Wire Line
	2900 4950 3150 4950
Wire Wire Line
	3150 4950 3150 5100
Connection ~ 2900 4950
Wire Wire Line
	3850 5500 3950 5500
Connection ~ 3950 5500
Connection ~ 1100 6450
Wire Wire Line
	1100 6450 1100 6050
Wire Notes Line
	4550 5750 2450 5750
Wire Notes Line
	4550 4700 2450 4700
Wire Notes Line
	4550 4700 4550 5750
Wire Notes Line
	2450 4700 2450 5750
Text GLabel 3300 6000 1    50   Input ~ 0
INT1
Text Notes 4050 4950 0    50   Italic 10
5V Pulse\nInput
Text Notes 1550 4850 0    50   Italic 10
3V3 Pulse Input
$Comp
L Device:R_Small R24
U 1 1 636C91B5
P 10300 4000
F 0 "R24" V 10300 3350 50  0000 L CNN
F 1 "10" V 10300 3950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10300 4000 50  0001 C CNN
F 3 "~" H 10300 4000 50  0001 C CNN
	1    10300 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	10400 4000 10750 4000
Wire Wire Line
	5300 7450 6200 7450
Wire Wire Line
	5300 7550 6200 7550
$EndSCHEMATC
